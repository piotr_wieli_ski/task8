﻿var Module = (function () {

    function getCurrencyRates() {

        $.getJSON("/Home/GetExhangeRates", setValue);
    }

    function setValue(data) {
        for (var i = 0; i < data.length; i++) {
            appViewModel.currencyArray.push(data[i]);
        }
    }

    return {
        getCurrencyRates: getCurrencyRates
    }

})();

function AppViewModel() {
    var self = this;
    self.getCurrencyRates = function () {
        Module.getCurrencyRates();
    }
    self.currencyArray = ko.observableArray([]);
};

var appViewModel = new AppViewModel();
ko.applyBindings(appViewModel);
