﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using CurrencyRates.Interfaces;
using CurrencyRates.ViewModels;
using CurrencyRates.Model;

namespace CurrencyRates.Services
{
    public class CurrencyService : ICurrencyService
    {
        public void GetCurrencyRates(ref List<CurrencyViewModel> list)
        {
            var request = WebRequest.Create(Consts.URLConsts.CurrencyRateSiteURL);
            var response = request?.GetResponse() as HttpWebResponse;
            if (response == null || response.StatusCode != HttpStatusCode.OK)
                return;
            var data = Deserialize<Web_dis_rates>(response);

            list = data.Row.Where(r => r.Swift_code == "USD" || r.Swift_code == "EUR")
                .Select(p => new CurrencyViewModel
                {
                    Currency = p.Swift_name,
                    BuyCash = p.Buy_cash,
                    SellCash = p.Sell_cash
                })
                .ToList();

        }

        public T Deserialize<T>(HttpWebResponse response)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Web_dis_rates));
            var stream = response.GetResponseStream();
            if (stream != null)
            {
                var res = (T)serializer.Deserialize(stream);

                return res;
            }

            return default(T);
        }
    }
  
}