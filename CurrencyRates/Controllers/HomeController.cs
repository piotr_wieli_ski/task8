﻿using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;
using CurrencyRates.Interfaces;
using CurrencyRates.ViewModels;

namespace CurrencyRates.Controllers
{
    public class HomeController : Controller
    {
        private ICurrencyService _currencyService;

        public HomeController(ICurrencyService currencyService)
        {
            _currencyService = currencyService;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetExhangeRates()
        {
            List<CurrencyViewModel> list = new List<CurrencyViewModel>();
            _currencyService.GetCurrencyRates(ref list);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
       
    }
}