﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CurrencyRates.ViewModels
{
    public class CurrencyViewModel
    {
        public string Currency { get; set; }
        public string SellCash { get; set; }
        public string BuyCash { get; set; }
    }
}